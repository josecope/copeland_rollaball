﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//tell the script which systems are being used
public class PlayerController : MonoBehaviour
{
    //Creates nodes for the script components so that speed and the texts can be edited from Unity
    public float speed;
    public Text countText;
    public Text winText;

    private Rigidbody rb;
    private int count;

    public Vector3 originalPosition { get; private set; }
    //added an original position for the script to identify

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        SetCountText();
        winText.text = "";
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        //NEW FEATURE!!! --- Jumping
        if (Input.GetKeyDown(KeyCode.Space)) //tells the code that if the space key is pressed that something needs to happen
        {
            if (transform.position.y <= 1) //dictates that the y coordinate must be less than or equal to 1 so that the player cannot jump repeatedly
            {
                GetComponent<Rigidbody>().AddForce(Vector3.up * 300); //calls the rigid body of the player sphere and applies upward force to it
            }
        }

        //NEW FEATURE!!! --- Respawn
        if (transform.position.y <= -5) //says that if the y coordinate is less than -5 something needs to happen
        {
            transform.position = originalPosition; //says that the player needs to be transported back to the original position, aka the spawn point
            rb.velocity = Vector3.zero; //sets the velocity back to 0 after the player respawns
        }

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        rb.AddForce(movement * speed);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag ("Pickups"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;
            SetCountText();
        }
    }

    void SetCountText ()
    {
        countText.text = "Count: " + count.ToString();
        if (count >= 10)
        {
            winText.text = "You Win!";
        }
    }
}





/*  IDEAS FOR MODIFICATIONS
 *  sphere gets bigger when it collects pickups
 *  confetti blast when the player wins
 *  pickups travel on a trail around the area
 *  sphere goes from red to green as you collect more and more pickups
 *  change ground and wall materials
 *  expand play area
 *  paths
 *  jump feature for the sphere
 *  light up the world
 *  add more pickups
 *  different height levels
 *  spinning sections of the map
 *  better win text
 *  play again option
 *  ramp/big jump for the player
 *  */